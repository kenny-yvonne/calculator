import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    @Test
    public void testAddWithComma() {
        Integer expected = 2;
        Integer actual = new Calculator().add("1,1");

        assertEquals(expected, actual, "the addition of \"1,1\" should be 2");
    }

    @Test
    public void testAddWithNewLine() {
        Integer expected = 5;
        Integer actual = new Calculator().add("1\n2,2");

        assertEquals(expected, actual, "addition of \"1\n2,2\" should be 5");
    }

}
